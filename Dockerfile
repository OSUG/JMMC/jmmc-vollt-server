# buid process could take place here
# https://codefresh.io/docs/docs/learn-by-example/java/gradle/ROM gradle:4.7.0-jdk8-alpine AS build
FROM gradle:6.8.0-jdk8 AS builder

RUN git clone https://github.com/gmantele/vollt.git 
WORKDIR vollt

# next branch is not approved by author but seems to be ok at present time for our needs
# this image will be reproducible after using a tag instead of a branch ;)
RUN git checkout adql2.1
RUN sed -i "s/DEFAULT_VERSION = ADQLVersion.V2_0/DEFAULT_VERSION = ADQLVersion.V2_1/g" ADQLLib/src/adql/parser/ADQLParser.java
RUN grep "DEFAULT_VERSION =" ADQLLib/src/adql/parser/ADQLParser.java

RUN gradle jar

#RUN find /home

# run time image
# https://hub.docker.com/_/tomcat
# WARNING: xml ns's for config files depends on tomcat version
FROM tomcat:7-jdk8-openjdk-slim-buster

# install our empty vollt app so we query on xxx/vollt/tap/xxx urls
WORKDIR $CATALINA_HOME/webapps
COPY vollt vollt
# vollt
#   ├── META-INF
#   │   └── context.xml
#   └── WEB-INF
#       ├── lib
#       ├── tap.properties
#       └── web.xml


# and complete with missing jars that have been built previously
COPY --from=builder /home/gradle/vollt/lib/*.jar vollt/WEB-INF/lib/
COPY --from=builder /home/gradle/vollt/*/build/libs/*.jar vollt/WEB-INF/lib/
# + postgres driver
ADD https://jdbc.postgresql.org/download/postgresql-42.2.18.jar vollt/WEB-INF/lib/

RUN ls -lrt



