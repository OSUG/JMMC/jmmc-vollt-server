# TAP server 
Build a working but customizable TAP server on top of vollt!
https://github.com/gmantele/vollt/tree/tapLib-2.3


Webapp exposed on container:8080/vollt/tap/ with 

Build with gradle image  nd executed with a (old) tomcat

# Build
- docker build -t vollt .

# Execution 
Your db server must be declared in tap.properties (or context.xml for jndi)
- docker run --rm --name vollt -p 8008:8080 vollt

Or defined at runtime with the ip of the server, e.g.:
- docker run --add-host=db:192.168.1.117 --rm --name vollt -p 8008:8080 vollt
- docker run --add-host=db:172.19.4.1 --rm --name vollt -p 8008:8080 vollt

Logs are stored in `/tap` directory

# Customization
Web app is stored under `/usr/local/tomcat/webapps/vollt`

Main config:
- db server,db name, pool... are defined in `META-INF/context.xml`

Other params can be adjusted :
- in `WEB-INF/web.xml` for the webapp
- in `WEB-INF/tap.properties` for its configuration 


